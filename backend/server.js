var dbFile = "db/test.db";

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('config/properties.file');
var max = properties.get('max');

var express = require('express')
var bodyParser = require('body-parser')

var app = express()

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var server = app.listen(8080, function() {
    console.log("Server listening on: http://localhost:8080");
});

var io = require('socket.io').listen(server);

app.get('/', function(req, res) {
    /* Database configuration */
    var sqlite3 = require('sqlite3').verbose();
    var db = new sqlite3.Database(dbFile);

    var comments = [];
    db.serialize(function() {
        db.each("SELECT * FROM comments", function(err, row) {
            comments.push(row);
        }, function() {
            res.end(JSON.stringify({
                max: max,
                comments: comments
            }));
        })
    })

    db.close();
})

app.post('/post-comment', jsonParser, function(req, res) {
    console.log('body:', req.body);

    /* Database configuration */
    var sqlite3 = require('sqlite3').verbose();
    var db = new sqlite3.Database(dbFile);

    db.serialize(function() {
        var createSql = "CREATE TABLE IF NOT EXISTS comments (id INTEGER, message TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (id))";
        db.run(createSql);

        db.get("SELECT count(*) as comments_no FROM comments", function(err, row) {
            if(row.comments_no == max) {
                var sqlite3 = require('sqlite3').verbose();
                var db = new sqlite3.Database(dbFile);

                db.run("DELETE FROM comments WHERE id IN (SELECT id FROM comments ORDER BY id LIMIT 1)");
            }
        })

        var sql = "INSERT INTO comments (message) VALUES ('" + req.body.message + "')";
        db.run(sql, function(param) {
            var sSql = "SELECT * FROM comments WHERE id = " + this.lastID;

            var sqlite3 = require('sqlite3').verbose();
            var db = new sqlite3.Database(dbFile);

            db.serialize(function() {
                db.get(sSql, function(err, row) {
                    io.emit('chat_message', {message: row});
                })
            })
        });
    });

    db.close();

    res.end();
})