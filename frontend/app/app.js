var myApp = angular.module('ChatApp', []);

myApp.controller('MainController', function($scope, $http, $location) {
    var nodeServerURL = 'http://localhost:8080';

    $scope.maxMessages = undefined;
    $scope.comments = undefined;

    var socket = io.connect(nodeServerURL);

    socket.on('chat_message', function(data){
        $scope.$apply(function() {
            if($scope.comments.length >= $scope.maxMessages) {
                $scope.comments.shift();
            }

            $scope.comments.push(data.message);
        })
    });

    var init = function() {
        $http({
            method: 'GET',
            url: nodeServerURL
        })
            .success(function (data, status, headers, config) {
                console.log('SUCCESS FUNCTION', data);
                $scope.maxMessages = data.max;
                $scope.comments = data.comments;
            })
            .error(function (data, status, headers, config) {
                console.log('ERROR FUNCTION', data);
            });
    }

    init();

    $scope.submitMessage = function() {
        $http({
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            url: nodeServerURL + '/post-comment',
            data: {
                message: $scope.message
            }
        })
            .success(function (data, status, headers, config) {
                console.log('SUCCESS FUNCTION', data);
                $scope.message = "";
            })
            .error(function (data, status, headers, config) {
                console.log('ERROR FUNCTION', data);
            });
    }
})